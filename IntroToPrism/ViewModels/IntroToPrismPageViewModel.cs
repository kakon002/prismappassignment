﻿using Prism.Mvvm;
using System.Diagnostics;

namespace IntroToPrism.ViewModels
{
    public class IntroToPrismPageViewModel : BindableBase
    {
        public IntroToPrismPageViewModel()
        {
            Debug.WriteLine($"**** {this.GetType().Name}: ctor");
        }
    }
}